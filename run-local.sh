#!/usr/bin/env bash

docker network create proxy_traefik
export HOST=dev.local
docker compose -f docker-compose.yml -f docker-compose.local.yml up -d