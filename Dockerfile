FROM traefik:latest

COPY traefik_https.toml /etc/traefik/traefik.toml
RUN mkdir /etc/traefik/acme