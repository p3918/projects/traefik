#!/usr/bin/env bash

docker network create proxy_traefik
if [ "$1" == "https" ]
then
  docker compose -f docker-compose.yml -f docker-compose.https.yml up -d
else
  docker compose up -d
fi